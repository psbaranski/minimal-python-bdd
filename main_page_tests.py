import unittest
from selenium import webdriver
import chromedriver_autoinstaller
from selenium.webdriver.chrome.service import Service


class MainPageTests(unittest.TestCase):
    def setUp(self):
        print('setUp')
        chromedriver_autoinstaller.install()
        self.driver = webdriver.Chrome(service=Service())

        self.base_url = 'https://antoogle.testoneo.com/'

    def tearDown(self):
        print('tearDown')
        self.driver.quit()

    def test_main_page_title(self):
        expected_title = 'The Antoogle Search Page'
        self.driver.get(self.base_url)
        actual_title = self.driver.title
        self.assertEqual(expected_title, actual_title)
