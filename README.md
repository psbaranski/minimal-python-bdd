# Setup
   1. Add venv to project
   2. Install requirements 
# Run traditional tests:
   1. Run main_page_tests file
      1. `python -m unittest main_page_tests.MainPageTests`
# Run BDD in console:
   1. `behave`
   2. With string printing
      1. ` behave  --no-capture`
   3. For running tests on WINDOWS OS 
      1. Chrome headless mode is disabled
      2. See implementation of step "I enter the main Antoogle page"
# Debug BDD in PyChram (menu):
   1. Run
   2. Edit Configuration
   3. `+` (Python)
   4. Name i.e. : `feature 1`
   5. Change setting: `Script path` to `Module name`
   6. Add value to `Module name` setting: `behave`
   7. Add value to `Parameters` setting: 
      1. `$ProjectFileDir$\features --no-capture`
   8. How it should look:
      1. ![debug_conf.png](debug_conf.png)
   9. Save
   10. Add brake point to feature implementation in `main_page_steps.py`
       1. Unleash debugging power