import os

import chromedriver_autoinstaller
from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@given(u'The Antoogle site is up and running')
def step_impl(context):
    print("Hello BDD!")


@when(u'I enter the main Antoogle page')
def step_impl(context):
    chromedriver_autoinstaller.install()

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')

    if os.name != 'nt':
        chrome_options.add_argument('--headless')

    context.driver = webdriver.Chrome(service=Service(), options=chrome_options)
    context.driver.get('https://antoogle.testoneo.com/')


@then(u'I see The Antoogle Search Page as a page title')
def step_impl(context):
    expected_title = "The Antoogle Search Page"
    title = context.driver.title
    assert title == expected_title, f"Actual: {title}, expected: {expected_title}"
    context.driver.quit()
